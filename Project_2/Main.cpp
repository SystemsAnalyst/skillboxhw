#include "DynamicArrays/CustomStack.h"
#include "DynamicArrays/CustomStack.cpp"
#include "Inheritance/Cow.h"
#include "Inheritance/Dog.h"
#include "Inheritance/Animal.h"
#include "Inheritance/Tiranosaur.h"
#include "Overloading/Vector.h"
#include "Constructors/ServerRoom.h"
#include "ObjectRelations/Car.h"
#include "ObjectRelations/Bicycle.h"
#include "ObjectRelations/RubberBoat.h"

#include "Exceptions/Fraction.h"

// Common dependencies
#include <iostream>  
#include <vector>  


#define LF "\n"

void testInheritance(void);
void testDynamicArray(void);
void testOverloading(void);
void testCopyConstructors(void);
void testObjectRelations(void);
void testExceptions(void);


int main(){
	setlocale(LC_ALL, "rus");

/*
	// �������� ������������� �������
	std::cout << "----------------------- DYNAMIC ARRAYS ----------------------- BEGIN\n";
	testDynamicArray();
	std::cout << "----------------------- DYNAMIC ARRAYS ----------------------- END\n";

	// �������� ������������
	std::cout << "\n----------------------- INHERITANCE ----------------------- BEGIN\n";
	testInheritance();
	std::cout << "----------------------- INHERITANCE ----------------------- END\n";

	// �������� ���������� ����������
	std::cout << "\n\n----------------------- OVERLOADING ----------------------- BEGIN\n";
	testOverloading();
	std::cout << "----------------------- OVERLOADING ----------------------- END\n";

	// ������������ �����������
	std::cout << "\n\n----------------------- COPY CONSTRUCTORS ----------------------- BEGIN\n";
	testCopyConstructors();
	std::cout << "----------------------- COPY CONSTRUCTORS ----------------------- END\n";

	std::cout << "\n\n----------------------- ��������� ����� ��������� � ����������� ������� ----------------------- BEGIN\n";
	testObjectRelations();
	std::cout << "----------------------- ��������� ����� ��������� � ����������� ������� ----------------------- END\n";

*/

	std::cout << "\n\n----------------------- ���������� ----------------------- BEGIN\n";
	testExceptions();
	std::cout << "----------------------- ���������� ----------------------- END\n";

}


/*�������� ����� Fraction, ���������� ����������-����� numerator � denominator. 
���� ��� �������� ������� � �������� denominator ������� 0, �� ������ ����������� ���������� ���� std::runtime_error. 
� ������� main() ��������� ������������ ������ ��� ����� �����. 
���� ����� ������������, �� �� ������ ������������ ���������� ���� std::exception � �������� ������������, ��� �� ���� ������������ ������.
�������� ������ �� ����������� � �������� ��� �������� �������� ������.
*/
void testExceptions(void) {

	long num;
	long denum;

	char repeat;

	do {
		repeat = 'N';
		std::cout << "������� ��������� (�������): \n";
		std::cin >> num;
		std::cout << "������� ����������� (��������): \n";
		std::cin >> denum;

		try {
			Fraction frac = Fraction(num, denum);
		}
		catch (std::runtime_error& exc) {
			std::cerr << "������! " << exc.what() << "\n";
		}

		std::cout << "��� ���? 'Y' - ���������.\n";
		std::cin >> repeat;

	} while (repeat == 'Y' || repeat == 'y');
}




/** 4.11 �������� ������
�������� ����������� ����� Vehicle, ���������� ��������� ������:
    ������ ����������� ������� print() � ���������� ���� std::ostream;
    ���������� operator<<;
�������� ��� ������ WaterVehicle � RoadVehicle, ������� ��������� ����� Vehicle.
    WaterVehicle ������ ����� ������ ���� float � �������� ����������-�����;
    RoadVehicle ������ ����� �������� ������� ���� float � �������� ����������-�����;
�������� ��� ������ Bicycle � Car, ������� ��������� ����� RoadVehicle:
    Bicycle ������ ����� ��� ������ ������ Wheel � �������� ����������-������;
    Car ������ ����� 4 ������ ������ Wheel � ��������� ������ Engine � �������� ����������-�����;
�������� ��� ������ Wheel � Engine:
    Wheel ������ ����� ������� ���� float � �������� ����������-�����;
    Engine ������ ����� �������� ���� float � �������� ����������-�����;

����������� ������� print(), ����� ��������� ��� �������� ��������� �����:
    #include <iostream>
    int main(){
    Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);
    std::cout << c << '\n';
    Bicycle t(Wheel(20), Wheel(20), 300);
    std::cout << t << '\n';
    return 0;
   }

���������� ��������� ���������:
    Car Engine: 150 Wheels: 17 17 18 18 Ride height: 150
    Bicycle Wheels: 20 20 Ride height: 300
���������� ���������� ��������� � TODO ���������� ����:
    #include <iostream>
    #include <vector>
    int main(){
       std::vector<Vehicle*> v;
       v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));
       v.push_back(new Circle(Point(1, 2, 3), 7));
       v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));
       v.push_back(new WaterVehicle(5000));
       //TODO: ����� ��������� ������� v �����
       std::cout << "The highest power is" << getHighestPower(v) << '\n'; // ���������� ��� �������
       //TODO: �������� ��������� ������� v �����
    }
*/
float getHighestPower(const std::vector<Vehicle*> vector) {
	float MaxPower = 0.f;
	for (Vehicle* element : vector) {
		if (element != nullptr) {
			MaxPower = fmaxf(MaxPower, element->getEnginePower());
		}
	}

	return  MaxPower;
}

void testObjectRelations(void) {
	std::cout << "\n --- ���������� ������� �� ������ ---- \n";
	Car car_1(new Engine(150), 150, new Wheel(17), new Wheel(17), new Wheel(18), new Wheel(18));
	std::cout << car_1;
	Bicycle bicycle(200, new Wheel(20), new Wheel(20));
	std::cout << bicycle;

	std::cout << "\n --- ���������� ������� �� ��������� ---- \n";
	std::vector<Vehicle*> vector;
	vector.push_back(new Car(new Engine(150), 250, new Wheel(17), new Wheel(17), new Wheel(18), new Wheel(18)));
	vector.push_back(new Car(new Engine(200), 130, new Wheel(19), new Wheel(19), new Wheel(19), new Wheel(19)));
	vector.push_back(new Bicycle(200, new Wheel(20), new Wheel(20)));
	vector.push_back(new WaterVehicle(200, 600));

	//TODO: ����� ��������� ������� v �����
	for (Vehicle* element : vector) {
		if (element != nullptr) {
			std::cout << *element;
		}
	}

	std::cout << "\n\nThe highest power is " << getHighestPower(vector) << " Horse Power\n";
	
	std::cout << "\n --- �������� ��������� ������� ---- \n"; 
	//TODO: �������� ��������� ������� v �����
	for (Vehicle* element : vector) {
		if (element != nullptr)
			delete element;
	}

	std::cout << "\n --- �������� ��������� ���������� ---- \n";
}



/**
���� ������ � ������� ���� ����������� �����, 
� ������� ����� ��������� ��������� ������������ ������ ������ ����, 
� ����� ��������� �������������� ��������� ����� ������ ��� ������ ����������. 
� ������������ ���������������� ��� ������ ��������c��. 
������� ���, ����� ��� ����������� ������� ������� ������ ����������� �������� �����������.
*/
void testCopyConstructors(void) {
	ServerRoom* room1 = new ServerRoom("Orange White", 500, 2, 16);
	room1->AddServer(0, "DELL PseudoPower", 32, 4, 1.2f);
	room1->AddServer(1, "Yandex Cloud Compute", 64, 8, 4.3f);
	room1->AddServer(1, "Elbrus E2K ", 128, 8, 3.3f);
	room1->AddServer(1, "Elbrus E4K ", 256, 16, 8.1f);
	room1->AddRacks(2);
	room1->AddServer(2, "IBM Stupid Power", 4, 1, 12.7f);

	ServerRoom* room2 = new ServerRoom(*room1);
	std::cout << "\n��������� ������ Room1:\n" << *room1 << LF;
	std::cout << "\n��������� ������ Room2:\n" << *room2 << LF;

	std::cout << "\n������ Room1:\n";
	delete room1;
	std::cout << "\n�������� ��������� ������ Room2:\n" << *room2 << LF;

	std::cout << "\n������ Room2:\n";
	delete room2;
}


void testOverloading(void) {
	Vector v_a = Vector(2.0f, 4.0f, 8.0f);
	Vector v_b = Vector(-3.0f, 5.0f, -7.0f);
	Vector v_c = Vector(11.0f, -7.0f, 15.0f);
	Vector v_in;

	std::cout << "Enter 3 components ot the Vector \n";
	std::cin >> v_in;
	std::cout << "Entered Vector: " << v_in << LF;

	std::cout << "Vector A: " << v_a << LF;
	std::cout << "Vector B: " << v_b << LF;
	std::cout << "Vector C: " << v_c << LF;

	std::cout << "Vector C+A = " << v_a + v_c << LF;
	std::cout << "Vector B-C = " << v_b - v_c << LF;
	std::cout << "Scalar Product A*B = " << v_a * v_b << LF;
	std::cout << "Entered Vector * PI = " << v_in * PI << LF;

	std::cout << "Vector A == B ? " << (v_a == v_b) << LF;
	std::cout << "Vector A > B ? " << (v_a > v_b) << LF;
}


void testInheritance(void) {
	Animal* myAnimals[10];
	int animalsCount = 0;

	myAnimals[animalsCount++] = new Cow("Rogatus Simplus", "Zorka");
	myAnimals[animalsCount++] = new Dog("Cannus Hvostus Gavkus", "Bobik");
	myAnimals[animalsCount++] = new Tiranosaur("Tiranosaur REXX", "Zubastik");

	std::cout << "My Animals: \n";
	for (int i = 0; i < animalsCount; i++) {
		std::cout << i+1 << ":  " << myAnimals[i]->whoAmI() << "  says " << myAnimals[i]->voice() << LF;
	}

	for (int i = 0; i < animalsCount; i++) {
		delete myAnimals[i];
		myAnimals[i] = nullptr;
	}
	animalsCount = 0;
}


void testDynamicArray(void) {
	// �������� ���������� � ������������� ����
	CustomStack<int> stack(0, 50);

	// �������� ������ ��� ���������� �����
	int tempArray[] = { 22,33,675,7534,7885,3326 };

	std::cout << "Stack initialized \n";
	std::cout << "Stack Size= " << stack.depth() << LF;

	// �������� ����
	for (int element : tempArray) {
		stack.push(element);
		std::cout << "Push element: " << element << LF;
	}

	std::cout << "Stack Size= " << stack.depth() << LF;
	std::cout << "Stack Elements: \n";

	// �������� �� �����
	while (stack.depth() > 0) {
		std::cout << "Pop element: " << stack.pop() << LF;
	}

	std::cout << "FINAL Stack Size= " << stack.depth() << LF;
}
