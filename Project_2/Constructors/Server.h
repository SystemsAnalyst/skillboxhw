#pragma once
#include <iostream>  

class Server {
public:
	Server();
	Server(Server& SourceServer);
	Server(std::string DefaultName, int DefaultMemory, int DefaultCoreCount);
	~Server();
	int GetId(void);

	friend std::ostream& operator<<(std::ostream& out, const Server& ServerInstance);

	std::string Name;
	int Memory;
	int CoreCount;

private:
	int Id;
};

