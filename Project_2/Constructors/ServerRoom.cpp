#include "ServerRoom.h"


ServerRoom::ServerRoom() {
	InitializeRoom("New Server Room", 1000, 4, 16);
}


ServerRoom::ServerRoom(std::string Name, float MaxPower, int InitialRackCount, int InitialRackSize){
	InitializeRoom(Name, MaxPower, InitialRackCount, InitialRackSize);
}


void ServerRoom::InitializeRoom(std::string Name, float MaxPower, int InitialRackCount, int InitialRackSize) {
	RoomName = Name;
	PowerAllocated = 0;
	PowerMaxAllowed = MaxPower;
	RackCountMax = 0;
	RackCount = 0;
	RackSize = InitialRackSize;
	RacksArray = 0;
	AddRacks(InitialRackCount);
}


ServerRoom::ServerRoom(ServerRoom& SourceRoom) {
	InitializeRoom(SourceRoom.RoomName, SourceRoom.PowerMaxAllowed, SourceRoom.RackCount, SourceRoom.RackSize);
	this->PowerAllocated = SourceRoom.PowerAllocated;

	for (int rack_id = 0; rack_id < RackCount; rack_id++) {
		Server** SourceRack = SourceRoom.RacksArray[rack_id];
		Server** TargetRack = this->RacksArray[rack_id];
		for (int slot = 0; slot < RackSize; slot++) {
			if (SourceRack[slot] != 0)
				TargetRack[slot] = new Server(*SourceRack[slot]);
		}
	}
}


ServerRoom::~ServerRoom() {
	std::cout << "-----------Server Room Destructor------------" << this << "\n";
	for (int rack_id = 0; rack_id < RackCount; rack_id++){
		Server** TempRack = RacksArray[rack_id];
		for (int slot = 0; slot < RackSize; slot++) {
			if (TempRack[slot] != 0)
				delete TempRack[slot];
		}
		delete[] TempRack;
	}
	delete[] RacksArray;
}


void ServerRoom::AddServer(int RackId, std::string Name, int Memory, int Cores, float Power) {
	Server** Rack = RacksArray[RackId];
	for (int slot_id = 0; slot_id < RackSize; slot_id++) {
		if (Rack[slot_id] == 0) {
			Rack[slot_id] = new Server(Name, Memory, Cores);
			PowerAllocated += Power;
			break;
		}
	}

}


std::ostream& operator<<(std::ostream& out, const ServerRoom& Room) {
	out << "SERVER ROOM NAME=[" << Room.RoomName.c_str() << "]  RACK COUNT=[" << Room.RackCount << "]  POWER ALLOCATED=[" << Room.PowerAllocated << " / " << Room.PowerMaxAllowed << " kW/h]\n";
	for (int rack_id = 0; rack_id < Room.RackCount; rack_id++) {
		out << "   RACK=[ " << rack_id << " ]\n";
		Server** Rack = Room.RacksArray[rack_id];
		for (int slot_id = 0; slot_id < Room.RackSize; slot_id++) {
			if (Rack[slot_id] != 0) {
				out << *Rack[slot_id];
			}
		}
	}
	return out;
}

// �������� ������ � �������
void ServerRoom::AddRacks(int NewCount) {
	// �������� ������� �� ������ ������
	Server*** RacksOld = RacksArray;

	// ������ ����� ������
	RacksArray = new Server**[RackCount+NewCount];
	// �������� ������ �� ������� ������� � �����
	for (int rack_id = 0; rack_id < RackCount; rack_id++) 
		RacksArray[rack_id] = RacksOld[rack_id];
	// ������ ����� ������
	for (int rack_id = RackCount; rack_id < RackCount + NewCount; rack_id++) {
		RacksArray[rack_id] = new Server*[RackSize];
		Server** TempRack = RacksArray[rack_id];
		for (int slot = 0; slot < RackSize; slot++)
			TempRack[slot] = 0;
	}

	RackCount = RackCount + NewCount;
	delete[] RacksOld;
}