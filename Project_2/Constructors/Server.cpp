#include <stdlib.h>
#include "Server.h"


Server::Server() {
	Id = Server::GetId();
	Name = "Default Server";
	Memory = 16;
	CoreCount = 1;
}


Server::Server(Server& SourceServer) {
	this->Id = SourceServer.Id;
	this->Name = SourceServer.Name;
	this->Memory = SourceServer.Memory;
	this->CoreCount = SourceServer.CoreCount;
}


Server::Server(std::string DefaultName, int DefaultMemory, int DefaultCoreCount) {
	Id = Server::GetId();
	Name = DefaultName;
	Memory = DefaultMemory;
	CoreCount = DefaultCoreCount;
}


Server::~Server() {
}


int Server::GetId(void) {
	return rand();
}


std::ostream& operator<<(std::ostream& out, const Server& ServerInstance) {
	out << "      SERVER ID=[" << ServerInstance.Id << "]  NAME=[" << ServerInstance.Name.c_str() << "]  RAM=[" << ServerInstance.Memory << " GB]  CORES=[" << ServerInstance.CoreCount << "]\n";
	return out;
}



