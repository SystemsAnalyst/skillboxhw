#pragma once
#include <iostream>  
#include "Server.h"

class ServerRoom {
public:
	ServerRoom();
	ServerRoom(std::string Name, float MaxPower, int InitialRackCount, int InitialRackSize);
	ServerRoom(ServerRoom& SourceRoom);
	~ServerRoom();

	void AddRacks(int NewCount);
	void AddServer(int RackId, std::string Name, int Memory, int Cores, float Power);

	friend std::ostream& operator<<(std::ostream& out, const ServerRoom& Room);

	Server** operator[](int index) {
		if (RackCount == 0 || RacksArray == 0 || index < 0 || index > RackCount - 1)
			return 0;
		else
			return RacksArray[index];
	}

private:
	void InitializeRoom(std::string Name, float MaxPower, int InitialRackCount, int InitialRackSize);

	std::string RoomName;
	float PowerAllocated;
	float PowerMaxAllowed;

	int RackCountMax;
	int RackCount;
	int RackSize;

	Server*** RacksArray;
};