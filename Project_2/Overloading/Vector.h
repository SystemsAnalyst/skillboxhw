#pragma once
#include <iostream>  

const float PI = 3.14159265358f;


class Vector {
public:
	Vector() {
		x = y = z = 0.0f;
	}

	Vector(float x0, float y0, float z0) {
		x = x0;
		y = y0;
		z = z0;
	}


	friend Vector operator+(const Vector& a, const Vector& b);
	friend Vector operator*(const Vector& a, const Vector& b);
	friend Vector operator*(const Vector& a, const float number);
	friend Vector operator-(const Vector& a, const Vector& b);

	friend bool operator==(const Vector& a, const Vector& b);
	friend bool operator>(Vector& a, Vector& b);

	friend std::ostream& operator<<(std::ostream& out, const Vector& vector);
	friend std::istream& operator>>(std::istream& in, Vector& vector);

	operator float() {
		return sqrt(x*x + y * y + z * z);
	}

	float operator[](int index) {
		switch (index) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			std::cout << "index error";
			return 0;
		}
	}


private:
	float x;
	float y;
	float z;
};