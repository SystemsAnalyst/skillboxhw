#include "Vector.h"
#include <cmath>


// �������� ��������
Vector operator+(const Vector& a, const Vector& b) {
	return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

// ��������� ������� �� �����
Vector operator*(const Vector& a, const float number) {
	return Vector(a.x * number, a.y * number, a.z * number);
}

// ��������� ������������ ��������
Vector operator*(const Vector& a, const Vector& b) {
	return Vector(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}

// ��������� ��������
Vector operator-(const Vector& a, const Vector& b) {
	return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}


bool operator==(const Vector& a, const Vector& b) {
	return a.x == b.x && a.y == b.y && a.z == b.z;
}

bool operator>(Vector& a, Vector& b) {
	return (float)(a) > static_cast<float>(b);
}



std::ostream& operator<<(std::ostream& out, const Vector& vector) {
	out << "(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
	return out;
}


std::istream & operator>>(std::istream & in, Vector & vector){
	in >> vector.x;
	in >> vector.y;
	in >> vector.z;

	return in;
}

