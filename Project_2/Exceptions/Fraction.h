#pragma once

class Fraction {
public:
	long numerator;
	long denumerator;

	Fraction(long num, long denum);
};