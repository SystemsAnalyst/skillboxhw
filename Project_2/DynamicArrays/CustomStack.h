#pragma once

#define STACK_SIZE_INCREMENT 100


template <class T>
class CustomStack {
public:
	CustomStack(int initialStackSize = 0, int initialSizeIncrement = STACK_SIZE_INCREMENT);
	~CustomStack();

	void push(T parameter);
	T pop(void);
	int depth(void);

private:
	void reAllocate(void);

	T* stackArray;
	int stackSize;
	int stackIncrement;
	int pointer;
};