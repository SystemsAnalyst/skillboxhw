#include "CustomStack.h"


template <class T>
CustomStack<T>::CustomStack(int initialStackSize, int initialSizeIncrement) {
	stackArray = nullptr;
	stackSize = 0;
	pointer = -1;
	stackIncrement = initialSizeIncrement;

	if (initialStackSize > 0)
		reAllocate();
}

template <class T>
CustomStack<T>::~CustomStack() {
	if (stackArray != nullptr)
		delete[] stackArray;
}


template <class T>
void CustomStack<T>::push(T parameter)
{
	if (pointer == stackSize - 1)
		reAllocate();
	stackArray[++pointer] = parameter;
}

template <class T>
int CustomStack<T>::depth(void) {
	return pointer + 1;
}



template <class T>
T CustomStack<T>::pop(void) {
	if (pointer >= 0) {
		T result = stackArray[pointer--];
		return result;
	}
	else {
		throw 0;
	}
}

template <class T>
void CustomStack<T>::reAllocate() {
	stackSize += stackIncrement;

	T* tempArray = new T[stackSize];
	for (int i = 0; i < pointer; i++)
		tempArray[i] = stackArray[i];

	delete[] stackArray;
	stackArray = tempArray;
	tempArray = nullptr;
}