#pragma once
#include "WaterVehicle.h"

class RubberBoat: public WaterVehicle {
public:
	RubberBoat();
	~RubberBoat();

	std::ostream& print(std::ostream& out) override;
};