#pragma once
#include <iostream>  
#include "Engine.h"

class Vehicle abstract{
public:
	Engine* VehicleEngine;

public:
	Vehicle() {
		VehicleEngine = nullptr;
	}
	virtual ~Vehicle();
	virtual std::ostream& print(std::ostream& out);
	friend std::ostream& operator<<(std::ostream& out, Vehicle& vehicle);

	float getEnginePower();
};