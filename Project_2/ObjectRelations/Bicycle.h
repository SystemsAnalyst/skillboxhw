#pragma once
#include "RoadVehicle.h"

class Bicycle : public RoadVehicle {
public:
	Bicycle();
	Bicycle(int clearance, Wheel* ForwardWheel, Wheel* RearWheel);
	~Bicycle();

	std::ostream& print(std::ostream& out) override;

};