#include "WaterVehicle.h"

WaterVehicle::WaterVehicle(){
}

WaterVehicle::~WaterVehicle(){
	std::cout << "~WaterVehicle() ";
}

WaterVehicle::WaterVehicle(int draft, int loadCapacity) {
	Draft = draft;
	LoadCapacity = loadCapacity;
}


std::ostream& WaterVehicle::print(std::ostream& out) {
	out << "WaterVehicle ";
	// ������ ������� ������
	out << "Draft: " << Draft << "��, ";
	// ����� ����������������
	out << "Load Capacity: " << LoadCapacity << "��.";

	return out;
}
