#include "Car.h"

Car::Car(){
}


Car::~Car(){
	std::cout << "~Car() ";
}

Car::Car(Engine* NewEngine, int NewClearance, Wheel* FrontLeft, Wheel* FrontRight, Wheel* RearLeft, Wheel* RearRight) {
	VehicleEngine = NewEngine;
	Clearance = NewClearance;

	setUpWheelCount(4);
	setWheel(0, FrontLeft);
	setWheel(1, FrontRight);
	setWheel(2, RearLeft);
	setWheel(3, RearRight);
}

std::ostream& Car::print(std::ostream& out) {
	out << "Car ";
	RoadVehicle::print(out);
	out << "\n";
	return out;
}
