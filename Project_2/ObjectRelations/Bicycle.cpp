#include "Bicycle.h"

Bicycle::Bicycle() {
}

Bicycle::Bicycle(int clearance, Wheel* ForwardWheel, Wheel* RearWheel){
	setUpWheelCount(2);
	setWheel(0, ForwardWheel);
	setWheel(1, RearWheel);
	Clearance = clearance;
}


Bicycle::~Bicycle() {
	std::cout << "~Bicycle() ";
}



std::ostream& Bicycle::print(std::ostream& out) {
	out << "Bicycle ";
	RoadVehicle::print(out);
	out << "\n";
	return out;
}
