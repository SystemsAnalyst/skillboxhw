#include "Vehicle.h"

Vehicle::~Vehicle() {
	std::cout << "~Vehicle() \r\n";
}


std::ostream& Vehicle::print(std::ostream& out) {
	return out << "Vehicle::";
}


std::ostream& operator<<(std::ostream& out, Vehicle& vehicle){
	return vehicle.print(out);
}

float Vehicle::getEnginePower() {
	return (VehicleEngine == nullptr ? 0.f : VehicleEngine->PowerHP);
}
