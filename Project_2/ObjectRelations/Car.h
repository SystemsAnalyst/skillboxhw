#pragma once
#include "RoadVehicle.h"


class Car : public RoadVehicle {
public:
	Car();
	Car(Engine* NewEngine, int NewClearance, Wheel* FrontLeft, Wheel* FrontRight, Wheel* RearLeft, Wheel* RearRight);

	~Car();

	std::ostream& print(std::ostream& out) override;

};
