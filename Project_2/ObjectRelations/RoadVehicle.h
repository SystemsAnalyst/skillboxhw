#pragma once
#include "Vehicle.h"
#include "Wheel.h"


class RoadVehicle abstract : public Vehicle  {
public:
	int Clearance;
	int WheelCount;
	Wheel** Wheels;

public:
	RoadVehicle();
	virtual ~RoadVehicle();

	void setUpWheelCount(int wheelsCount);
	void setWheel(int wheelId, Wheel* wheel);
	std::ostream& print(std::ostream& out) override;

};

