#pragma once
#include "Vehicle.h"

class WaterVehicle : public Vehicle {
public:
	int Draft;
	int LoadCapacity;

public:
	WaterVehicle();
	WaterVehicle(int draft, int loadCapacity);
	virtual ~WaterVehicle();

	std::ostream& print(std::ostream& out) override;
};

