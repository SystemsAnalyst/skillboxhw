#include "RoadVehicle.h"


RoadVehicle::RoadVehicle() {
	Clearance = 0;
	WheelCount = 0;
	Wheels = nullptr;
}


RoadVehicle::~RoadVehicle(){
	std::cout << "~RoadVehicle() ";
	if (VehicleEngine != nullptr)
		delete VehicleEngine;

	if (Wheels != nullptr) {
		for (int i = 0; i < WheelCount; i++) {
			if (Wheels[i] != nullptr)
				delete Wheels[i];
		}
		delete[] Wheels;
	}
}


void RoadVehicle::setUpWheelCount(int wheelsCount) {
	if (wheelsCount > 0 && Wheels == nullptr) {
		Wheels = new Wheel*[wheelsCount];
		WheelCount = wheelsCount;
		for (int i = 0; i < WheelCount; i++) {
			Wheels[i] = nullptr;
		}
	}
}

void RoadVehicle::setWheel(int wheelId, Wheel* wheel) {
	if (wheelId >= 0 && wheelId < WheelCount && wheel != nullptr) {
		Wheels[wheelId] = wheel;
	}
}

std::ostream& RoadVehicle::print(std::ostream& out) {

	// ������ ������� �������� ���������, ���� ���� �����
	if(VehicleEngine != nullptr)
		out << "Engine: "<< VehicleEngine->PowerHP <<" HP, ";

	// ����� �������� ����
	if (WheelCount > 0) {
		out << "Wheels:";
		for (int i = 0; i < WheelCount; i++) {
			if (Wheels[i] != nullptr) {
				out << ' ' << Wheels[i]->Radius ;
			}
		}
		out << ", ";
	}

	// ��� ����� - ������� (�������� �������)
	out << "Clearance: " << Clearance << "��.";

	return out;
}

