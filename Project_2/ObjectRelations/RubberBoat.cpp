#include "RubberBoat.h"

RubberBoat::RubberBoat(){
}

RubberBoat::~RubberBoat(){
	std::cout << "~RubberBoat() ";

}

std::ostream& RubberBoat::print(std::ostream& out) {
	out << "RubberBoat ";
	WaterVehicle::print(out);
	out << "\n";
	return out;
}