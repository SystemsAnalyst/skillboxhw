#pragma once
#include "Animal.h"


class Tiranosaur : public Animal {
public:
	Tiranosaur(std::string  name, std::string nickName) : Animal(name, nickName) {};
	~Tiranosaur() { std::cout << "~Tiranosaur()"; }
	
	std::string voice(void) override;
};

