#pragma once
#include "Animal.h"


class Dog : public Animal {
public:
	Dog(std::string  name, std::string  nickName) : Animal(name, nickName) {};
	~Dog() { std::cout << "~Dog()"; }

	std::string  voice(void) override;
};

