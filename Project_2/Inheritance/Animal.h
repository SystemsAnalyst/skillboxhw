#pragma once
#include <cstdio>
#include <iostream>  
#include <string>

class Animal abstract {
public:
	Animal(std::string name, std::string nickName);
	virtual ~Animal() { std::cout << "~Animal()" << "\r\n"; }

	std::string whoAmI(void);
	virtual std::string voice(void);

private:
	std::string myName;
	std::string myNickName;
};