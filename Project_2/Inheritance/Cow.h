#pragma once
#include "Animal.h"


class Cow : public Animal {
public:
	Cow(std::string  name, std::string  nickName) : Animal(name, nickName) {};
	~Cow() { std::cout << "~Cow()"; }
	
	std::string voice(void) override;
};
